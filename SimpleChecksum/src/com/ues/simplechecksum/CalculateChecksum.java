package com.ues.simplechecksum;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CalculateChecksum {

	public static void main(String[] args) {
		//Create checksum for this file
		File file = new File(args[0]);
		 
		//Use MD5 algorithm
		MessageDigest md5Digest = null;
		try {
			//md5Digest = MessageDigest.getInstance("MD5");
			md5Digest = MessageDigest.getInstance("SHA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		//Get the checksum
		String checksum = null;
		try {
			checksum = Checksum.getFileChecksum(md5Digest, file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		//see checksum
		System.out.println(checksum);

	}

}
