package com.ues.simplechecksum.junit;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

import com.ues.simplechecksum.Checksum;

public class ChecksumTestCases1 {

	@Test
	public void testFile1() {
		//Create checksum for this file
		File file = new File(".\\temp\\test.txt");
		 
		//Use MD5 algorithm
		MessageDigest md5Digest = null;
		try {
			md5Digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		//Get the checksum
		String checksum = null;
		try {
			checksum = Checksum.getFileChecksum(md5Digest, file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		assertEquals(checksum, "cb78b9f5127f48a2a8d2a6ce56ec5bae");
		
	}

	@Test
	public void testFile2() {
		//Create checksum for this file
		File file = new File(".\\temp\\test2.txt");	
		
		//Use MD5 algorithm
		MessageDigest md5Digest = null;
		try {
			md5Digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		//Get the checksum
		String checksum = null;
		try {
			checksum = Checksum.getFileChecksum(md5Digest, file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		assertEquals(checksum, "d6ba571a2c372256e38670d0eea7e4f8");
	}
}
